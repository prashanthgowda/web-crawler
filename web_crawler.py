from crawler import Crawler
from database import Database


class WebCrawler:
    def __init__(self):
        self.crawler = Crawler()
        self.base_url = None
        self.db_conn = None
        self.max_links = 100

    def get_database_conn(self):
        db_name = self.crawler.get_base_url(self.base_url)
        self.db_conn = Database(db_name)
        self.db_conn.create_tables()
        self.db_conn.update_url(self.base_url)

    def get_url_status(self, url):
        return self.crawler.get_url_status(url)

    def crawl(self, url):
        url_status = self.get_url_status(url)
        if url_status != 200:
            status = 'Error: ' + str(url_status)
            self.db_conn.update_status(status, url)
            return status
        html_docs = self.crawler.get_html_docs(url)
        self.db_conn.update_html_doc(html_docs, url)
        links = self.crawler.get_links(url)
        for link in links:
            self.db_conn.update_url(link)
            self.db_conn.update_url_map(url, link)
        status = 'crawled'
        self.db_conn.update_status(status, url)
        return status

    def run(self, base_url):
        crawl_count = 0
        self.base_url = self.crawler.format_url(base_url)
        self.crawler.set_base_url(base_url)
        self.get_database_conn()
        # print('starting to crawl')
        while crawl_count <= self.max_links:
            if self.db_conn.get_pending_status_count() < 1:
                # print("No More links to Crawl")
                break
            url = self.db_conn.get_url()
            if url is None:
                # print("No More links to Crawl")
                break
            if base_url not in url:
                status = 'Different Domain'
                self.db_conn.update_status(status, url)
                continue
            crawl_count += 1
            # print('crawling ' + str(url) + ' count: ' + str(crawl_count))
            self.crawl(url)
        return 'URL crawled and stored in '\
            + self.crawler.get_base_url(self.base_url)+'.db'


if __name__ == '__main__':

    crawler = WebCrawler()
    print(crawler.run('localhost:8000'))
