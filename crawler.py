from bs4 import BeautifulSoup
from urllib.parse import urlparse
import requests


class Crawler:

    def __init__(self):
        self.base_url = None

    def set_base_url(self, url):
        url = self.format_url(url)
        self.base_url = self.get_base_url(url)
        return self.base_url

    def format_url(self, url):
        if url is None or url == '':
            return 'http://' + self.base_url
        if url[:2] == '//' and self.base_url.split('.')[1] in url:
            url = 'http:' + url
        if url.startswith('/'):
            url = self.base_url + url
        if not url.startswith('http') and url.startswith('www'):
            url = 'http://' + url
        if not url.startswith('http') and not url.startswith('www'):
            if self.base_url is not None and self.base_url in url:
                url = 'http://www.'+url
            elif self.base_url is not None:
                url = 'http://' + self.base_url + '/' + url
            else:
                url = 'http://' + url
        return url

    def get_request(self, url):
        try:
            url = self.format_url(url)
            request = requests.get(url)
        except requests.exceptions.ConnectionError as e:
            return 651
        except requests.exceptions.Timeout as e:
            return 504
        except requests.exceptions as e:
            return e

        return request

    def parse_url(self, url):
        return urlparse(url)

    def get_url_status(self, url):
        """get the status of the url"""
        url = self.format_url(url)
        request = self.get_request(url)
        if type(request) is int:
            return request
        return request.status_code

    def get_html_docs(self, url):
        if not url.startswith('http'):
            url = 'https://' + url
        if self.get_url_status(url) == 200:
            html_content = self.get_request(url)
            return html_content.text

        return False

    def get_links(self, url):

        html_docs = self.get_html_docs(url)
        soup = BeautifulSoup(html_docs, 'html.parser')
        links = set()
        for link in soup.find_all('a'):
            href = link.get('href')
            href = self.format_url(href)
            links.add(href)
        return links

    def get_base_url(self, url):
        parsed_url = urlparse(url)
        base_url = parsed_url.netloc
        return base_url
