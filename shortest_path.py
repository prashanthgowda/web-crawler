from database import Database
import networkx as nx


def shortest_path(base_url, url_a, url_b):
    db_conn = Database(base_url)
    graph = nx.DiGraph()
    rows = db_conn.get_url_map()
    for row in rows:
        from_url, to_url = row
        graph.add_edge(from_url, to_url)

    return nx.shortest_path(graph, url_a, url_b)


if __name__ == '__main__':
    print(shortest_path('localhost:8000',
                        'http://localhost:8000', 'http://localhost:8000/index.html'))
