
###### Instructions:

**To use this Repo:**

1:git clone git@gitlab.com:PrashanthGowda/web-crawler.git
2:cd web-crawler

**To create a Virtual Environment**
mkvirtualenv venv

**To install the requirements run command**

pip install -r requirements.txt

**To crawl a website run command**

python3 web_crawler.py (default 'localhost:8000' will be crawled)

**to find shortest distance**

python3 shortest_path.py

**to run test on web-crawler**

host the test-web on localhost

bash test_crawler.sh