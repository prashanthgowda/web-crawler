import sqlite3


class Database:

    def __init__(self, domain='temp'):
        try:
            self.conn = sqlite3.connect('{}.db'.format(domain))
            self.domain = domain
            # self.create_tables()
        except sqlite3.Error as e:
            print("Database error: %s" % e)

    def execute_query(self, query, args=None):
        try:
            if args is None:
                result = self.conn.execute(query)
            else:
                result = self.conn.execute(query, args)
            self.conn.commit()
        except sqlite3.Error as e:
            return "Database error: %s" % e
        except Exception as e:
            return "Exception in _query: %s" % e
        return result

    def create_tables(self):
        content_table = "create table web_content (url text primary key, html_docs text, status text)"
        self.execute_query(content_table)
        url_map = "create table url_map (url_from text, url_to text)"
        status = self.execute_query(url_map)
        return status

    def update_url(self, url, status='pending'):
        param = (url, status)
        status = self.execute_query(
            'INSERT OR IGNORE INTO web_content (url,status) VALUES (?,?)', param)
        return status

    def get_url(self):
        url = self.execute_query(
            "select url from web_content WHERE status = 'pending' LIMIT 1")
        url = url.fetchone()[0]
        return url

    def update_html_doc(self, html_doc, url):
        result = self.execute_query(
            'update web_content SET html_docs = ? WHERE url = ?', (html_doc, url))
        return result

    def update_status(self, status, url):
        result = self.execute_query(
            'update web_content SET status = ? WHERE url = ?', (status, url))
        return result

    def update_url_map(self, url_from, url_to):
        result = self.execute_query(
            'INSERT OR IGNORE INTO url_map (url_from,url_to) VALUES (?,?)', (url_from, url_to))
        return result

    def get_link_count(self):
        result = self.execute_query('SELECT count(*) FROM web_content')
        count = result.fetchone()[0]
        return count

    def get_pending_status_count(self):
        result = self.execute_query(
            "SELECT count(*) FROM web_content where status='pending';")
        count = result.fetchone()[0]
        return count

    def get_url_map(self):
        result = self.execute_query("SELECT * FROM url_map;")
        return result.fetchall()

    def __del__(self):
        try:
            self.conn.commit()
            self.conn.close()
        except sqlite3.Error as e:
            return "Database error: %s" % e
        except Exception as e:
            return "Exception in _query: %s" % e
