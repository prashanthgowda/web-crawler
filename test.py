from web_crawler import WebCrawler
from shortest_path import shortest_path
import unittest
import os
import sqlite3


class WebCrawlerTestCases(unittest.TestCase):
    def execute_query(self, query, args=None):
        try:
            self.conn = sqlite3.connect('{}.db'.format('localhost:8000'))
            if args is None:
                result = self.conn.execute(query)
            else:
                result = self.conn.execute(query, args)
            self.conn.commit()
        except sqlite3.Error as e:
            return "Database error: %s" % e
        except Exception as e:
            return "Exception in _query: %s" % e
        return result

    def assert_html_doc_exist(self, c, res):
        pass

    def assert_link(self, expected_res):
        url = self.execute_query("select url from web_content")
        url = set(url.fetchall())
        self.assertEqual(url, set(expected_res))

    def assert_status(self):
        status = self.execute_query("select status from web_content")
        status_set = status.fetchall()
        for stat in status_set:
            self.assertNotEqual(stat[0], 'pending')

    def assert_url_map(self, expected_res):
        result = self.execute_query("SELECT * FROM url_map;")
        url_map = set(result.fetchall())
        self.assertEqual(url_map, set(expected_res))

    def assert_shortest_path(self, actual_result, expected_result):
        self.assertEqual(actual_result, expected_result)


class WebCrawlerTest(WebCrawlerTestCases):
    def tearDown(self):
        os.remove(os.getcwd() + '/localhost:8000.db')

    def test_crawler_links_fetch(self):
        crawler = WebCrawler()
        crawler.run('localhost:8000')
        expected_urls_in_webpages_table = [
            ('http://localhost:8000',),
            ('http://localhost:8000/1.html',),
            ('http://localhost:8000/2.html',),
            ('http://localhost:8000/3.html',),
            ('http://localhost:8000/4.html',),
            ('http://localhost:8000/5.html',),
            ('http://www.google.com',),
            ('http://localhost:8000/100.html',),
            ('http://localhost:8000/index.html',)
        ]
        self.assert_link(expected_urls_in_webpages_table)

    def test_crawler_status(self):
        crawler = WebCrawler()
        crawler.run('localhost:8000')
        self.assert_status()

    def test_crawler_url_map(self):
        crawler = WebCrawler()
        crawler.run('localhost:8000')
        expected_map = [
            ('http://localhost:8000',
             'http://localhost:8000/1.html'),
            ('http://localhost:8000/1.html',
             'http://localhost:8000/2.html'),
            ('http://localhost:8000/2.html',
             'http://localhost:8000/3.html'),
            ('http://localhost:8000/3.html',
             'http://localhost:8000/4.html'),
            ('http://localhost:8000/4.html',
             'http://localhost:8000/5.html'),
            ('http://localhost:8000/5.html',
             'http://localhost:8000/index.html'),
            ('http://localhost:8000/5.html',
             'http://www.google.com'),
            ('http://localhost:8000/5.html',
             'http://localhost:8000/100.html'),
            ('http://localhost:8000/index.html',
             'http://localhost:8000/1.html')]
        self.assert_url_map(expected_map)

    def test_shortest_path(self):
        crawler = WebCrawler()
        crawler.run('localhost:8000')
        actual_result = shortest_path('localhost:8000',
                                      'http://localhost:8000',
                                      'http://localhost:8000/index.html')
        expected_result = ['http://localhost:8000',
                           'http://localhost:8000/1.html',
                           'http://localhost:8000/2.html',
                           'http://localhost:8000/3.html',
                           'http://localhost:8000/4.html',
                           'http://localhost:8000/5.html',
                           'http://localhost:8000/index.html']
        self.assert_shortest_path(actual_result, expected_result)


if __name__ == '__main__':
    unittest.main()